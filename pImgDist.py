# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 13:21:28 2016
@author: johnlakness

Objectives:
Get distributions of probability of imaging versus time since last image for a set of arbitrary locations on earth

Tasks:
- Get satellite locations every 10 minutes
- turn each location into field of regard for last 10 minutes
- Determine whether it was looking at location
- build data structure for each point: last image time, dTs imaged / not imaged
"""

import math
import sympy
import sgp4,sgp4.io,sgp4.earth_gravity
import sidereal
import ecef
import numpy as np
import transformations as xfms
import datetime as dt
import time
import operator as op
import itertools as it
import shapefile
import csv

# create function to solve ellipsoid intersection
a,b = sympy.symbols("a,b")
t,x,y,z,dx,dy,dz = sympy.symbols("t,x,y,z,dx,dy,dz")
(X,Y,Z) = (x+t*dx,y+t*dy,z+t*dz)
ellipse = (X/a)**2+(Y/a)**2+(Z/b)**2-1
T=sympy.solve((ellipse,0),t)[1][0]#.subs(a,6378137).subs(b,6356752.3142)
fnT = sympy.lambdify(((a,b),(x,y,z),(dx,dy,dz)),T)
# wrapper for earth and returning LatLon
def fnEarthIntersect(position,vector,timestamp):
    (x,y,z) = position
    (dx,dy,dz) = vector
    (a,b)= (6378.137,6356.7523142)
    t = fnT((a,b),(x,y,z),(dx,dy,dz))
    lat,lon = ecef.ecef2geodetic(x+t*dx,y+t*dy,z+t*dz)
    rotation = sidereal.SiderealTime.fromDatetime(dt.datetime(*time.gmtime(0)[:6])).radians
    lon = (lon + math.degrees(rotation)+180) % 360 - 180
    return lat,lon
# apply roll angle (degrees) to downlooking vector
def fnRoll(position,velocity,theta):
    dv = np.matrix(list(position)+[0])
    dv = -1*dv/np.linalg.norm(dv,2)
    R = xfms.rotation_matrix(math.radians(theta),velocity)
    return (np.matrix(R)*dv.T).T[:,:3].tolist()[0]

with open('./datas/sat_reference.csv','r') as f:
    sensors = dict([(int(d['SSCNum']),{'hdx':int(d['Param1']),'hdy':int(d['Param2'])}) for d in csv.DictReader(f)])
    
with open('./datas/newtle.tce','r') as f:
    tlelines = f.readlines()    
rvd = it.groupby([sgp4.io.twoline2rv(tlelines[2*i],tlelines[2*i+1],sgp4.earth_gravity.wgs84) for i in range(int(len(tlelines)/2))],lambda rv:rv.satnum)
rvs = [rv for rv in [sgp4.io.twoline2rv(tlelines[2*i],tlelines[2*i+1],sgp4.earth_gravity.wgs84) for i in range(int(len(tlelines)/2))] if rv.satnum == 11]
rv_epochs = np.array([rv.epoch.timestamp() for rv in rvs])
times = np.arange(min(rv_epochs),max(rv_epochs),10*60)
rvi = np.searchsorted(rv_epochs,times) # find relevant TLE for each data line
pvs = [rv.propagate(*tuple(time.gmtime(ts))[0:6]) for ts,rv in zip(times,op.itemgetter(*rvi)(rvs))] # Task 1 output: satellite location every 10 minutes
theta  = 30 #max roll angle for field of regard
lls = [[fnEarthIntersect(p,fnRoll(p,v,r),t) for r in [-theta,theta]] for ((p,v),t) in zip(pvs,times)]
w = shapefile.Writer(shapefile.POLYGON)
for i in range(1,10):
    w.poly([[*lls[i-1],*lls[i][::-1]]])
    #w.field('TIMESTAMP',times[i])
    #w.field('SATNUM',rvs[rvi[i]].satnum)
    #w.record("{:d}_{:d}".format(rvs[rvi[i]].satnum,int(times[i])),'Polygon')
w.save('./shapefiles/FieldOfRegard.shp')