# -*- coding: utf-8 -*-

import math
import sympy
import sgp4,datetime,sidereal,ecef
import numpy as np
import transformations
import datetime as dt
import time
import operator as op


# create function to solve ellipsoid intersection
a,b = sympy.symbols("a,b")
t,x,y,z,dx,dy,dz = sympy.symbols("t,x,y,z,dx,dy,dz")
(X,Y,Z) = (x+t*dx,y+t*dy,z+t*dz)
ellipse = (X/a)**2+(Y/a)**2+(Z/b)**2-1
T=sympy.solve((ellipse,0),t)[1][0]#.subs(a,6378137).subs(b,6356752.3142)
fnT = sympy.lambdify(((a,b),(x,y,z),(dx,dy,dz)),T)
# wrapper for earth and returning LatLon
def fnEarthIntersect(x,y,z,dx,dy,dz):
    (a,b)= (6378.137,6356.7523142)
    t = fnT((a,b),(x,y,z),(dx,dy,dz))
    return ecef.ecef2geodetic(x+t*dx,y+t*dy,z+t*dz)
# apply roll angle (degrees) to downlooking vector
def fnRoll(position,velocity,theta):
    dv = np.matrix(list(position)+[0])
    dv = -1*dv/np.linalg.norm(dv,2)
    R = transformations.rotation_matrix(math.radians(theta),velocity)
    return (np.matrix(R)*dv.T).T[:,:3].tolist()[0]
  


# read in TLEs for International Space Station
iss = sgp4.io.twoline2rv("1 25544U 98067A   03097.78853147  .00021906  00000-0  28403-3 0  8652","2 25544  51.6361  13.7980 0004256  35.6671  59.2566 15.58778559250029",sgp4.earth_gravity.wgs84)
# compute position at event time (plug in event time for parameters)
#dt = datetime.datetime(2000,6,29,12,50,19)
position, velocity = iss.propagate(2000, 6, 29, 12, 50, 19)
# get look direction from roll angle of 1 degree
dv = fnRoll(position,velocity,1)
# get earth position in intertial reference frame
lat,lon = fnEarthIntersect(*position,*dv)
# correct position to rotating reference frame
lon = (lon + math.degrees(sidereal.SiderealTime.fromDatetime(dt).radians)+180) % 360 - 180


#test
(a,b)= (6378.137,6356.7523142)
position= (0,0,b+100)
velocity=(1,0,0)
theta = 45
dv = fnRoll(position,velocity,theta)
lat,lon = fnEarthIntersect(*position,*dv)
lon = (lon + math.degrees(sidereal.SiderealTime.fromDatetime(dt).radians)+180) % 360 - 180


with open('00011_PAN_RCD_FOV.sp','r') as f:
    header = [f.readline() for n in range(6)]
epoch_ref = time.strptime(header[3][16:-1],"%d %b %Y %H:%M:%S.%U0")

data = np.genfromtxt('00011_PAN_RCD_FOV.sp',skip_header = 6,skip_footer=1)
data[:,0] = data[:,0]+time.mktime(epoch_ref) # convert into common epoch

with open('newtle.tce','r') as f:
    tlelines = f.readlines()    
rvs = [rv for rv in [sgp4.io.twoline2rv(tlelines[2*i],tlelines[2*i+1],sgp4.earth_gravity.wgs84) for i in range(int(len(tlelines)/2))] if rv.satnum == 11]
rv_epochs = np.array([rv.epoch.timestamp() for rv in rvs])
rvi = rv_epochs.searchsorted(data[:,0]) # find relevant TLE for each data line
pvs = [rv.propagate(*tuple(time.gmtime(ts))[0:6]) for ts,rv in zip(data[:,0],op.itemgetter(*rvi)(rvs))]
lls = [fnEarthIntersect(*p,*fnRoll(p,v,r)) for ((p,v),r) in zip(pvs,data[:,3])]



#valid = np.where(list(map(all,zip(rvi!=0,rvi!=len(rvs)))))[0] # which data points have valid TLEs?
valid = np.where(data[:,0]>min(rv_epochs))[0] # which data points have valid TLEs?
valid_rvs = [rvs[rvi[i]] for i in valid]
pvs = [rv.propagate(*tuple(time.gmtime(ts))[0:6]) for ts,rv in zip(data[valid,0],valid_rvs)]
lls = [fnEarthIntersect(*p,*fnRoll(p,v,r)) for ((p,v),r) in zip(pvs,data[valid,3])]


"""
# not neccessary
def eci2ecef(x,y,z,dt):
    a = sidereal.SiderealTime.fromDatetime(dt).radians
    return (x*math.cos(a)+y*math.sin(a),-1*x*math.sin(a)+y*math.cos(a),z)
    #obsTime.year,obsTime.month,obsTime.day,obsTime.hour,obsTime.minute,obsTime.second)

def fnRot(X,P,V,theta):
    # https://docs.google.com/viewer?a=v&pid=sites&srcid=ZGVmYXVsdGRvbWFpbnxnbGVubm11cnJheXxneDoyMTJiZTZlNzVlMjFiZTFi
    (x,y,z),(a,b,c),(u,v,w) = X,P,V
    return [
        (a*(v**2+w**2)-u*(b*v+c*w-u*x-v*y-w*z))*(1-math.cos(theta))+x*math.cos(theta)+(-1*c*v+b*w-w*y+v*z)*math.sin(theta),
        (b*(u**2+w**2)-v*(a*u+c*w-u*x-v*y-w*z))*(1-math.cos(theta))+y*math.cos(theta)+( 1*c*u+a*w+w*x-u*z)*math.sin(theta),
        (c*(u**2+v**2)-w*(a*u+b*v-u*x-v*y-w*z))*(1-math.cos(theta))+z*math.cos(theta)+(-1*b*u+a*v-v*x+v*y)*math.sin(theta),        
        ]



def fnRot(theta,u,v):
    return np.matrix([[math.cos(theta) + u[0]**2 * (1-math.cos(theta)), 
         u[0] * u[1] * (1-math.cos(theta)) - u[2] * math.sin(theta), 
         u[0] * u[2] * (1 - math.cos(theta)) + u[1] * math.sin(theta)],
        [u[0] * u[1] * (1-math.cos(theta)) - u[2] * math.sin(theta),
         math.cos(theta) + u[1]**2 * (1-math.cos(theta)),
         u[1] * u[2] * (1 - math.cos(theta)) + u[0] * math.sin(theta)],
        [u[0] * u[2] * (1-math.cos(theta)) - u[1] * math.sin(theta),
         u[1] * u[2] * (1-math.cos(theta)) - u[0] * math.sin(theta),
         math.cos(theta) + u[2]**2 * (1-math.cos(theta))]])
    
"""