# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 16:42:02 2016

@author: johnlakness

Objective:
Create shapefiles with all imaged locations

Tasks:
1. read in TLEs
2. read in sensor parameters
3. read in roll angles
4. read in aperture openings
5. Form (satnum, open time, close time, roll angle)
6. compute bounds for each 10 second interval 
"""

import math
import sympy
import sgp4,sgp4.io,sgp4.earth_gravity
import sidereal
import ecef
import numpy as np
import transformations as xfms
import datetime as dt
import time
import operator as op
import itertools as it
import shapefile
import csv
import pandas as pd
import re

# create function to solve ellipsoid intersection
a,b = sympy.symbols("a,b")
t,x,y,z,dx,dy,dz = sympy.symbols("t,x,y,z,dx,dy,dz")
(X,Y,Z) = (x+t*dx,y+t*dy,z+t*dz)
ellipse = (X/a)**2+(Y/a)**2+(Z/b)**2-1
T=sympy.solve((ellipse,0),t)[1][0]#.subs(a,6378137).subs(b,6356752.3142)
fnT = sympy.lambdify(((a,b),(x,y,z),(dx,dy,dz)),T)
# wrapper for earth and returning LatLon
def fnEarthIntersect(position,vector,timestamp):
    (x,y,z) = position
    (dx,dy,dz) = vector
    (a,b)= (6378.137,6356.7523142)
    t = fnT((a,b),(x,y,z),(dx,dy,dz))
    lat,lon = ecef.ecef2geodetic(x+t*dx,y+t*dy,z+t*dz)
    rotation = sidereal.SiderealTime.fromDatetime(dt.datetime(*time.gmtime(0)[:6])).radians
    lon = (lon + math.degrees(rotation)+180) % 360 - 180
    return lat,lon
# apply roll angle (degrees) to downlooking vector
def fnRoll(position,velocity,theta):
    dv = np.matrix(list(position)+[0])
    dv = -1*dv/np.linalg.norm(dv,2)
    R = xfms.rotation_matrix(math.radians(theta),velocity)
    return (np.matrix(R)*dv.T).T[:,:3].tolist()[0]
# generate trace of footprint
def fnFootprint(apo,apc,rv,ra,hdx,hdy):
    tss = np.arange(apo,apc,10)
    pvs = [rv.propagate(*tuple(time.gmtime(ts))[0:6]) for ts in tss]
    dves = [[fnRoll(p,v,ra+a) for a in [-hdx,hdx]] for ts,(p,v) in zip(tss,pvs)] # roll extents of the FOV as downvectors
    lles = [[fnEarthIntersect(p,dv,ts) for dv in dve] for (p,v),dve,ts in zip(pvs,dves,tss)] # intersection with earth
    return [lle[0] for lle in lles] + [lle[1] for lle in lles[::-1]]

# sensor parameters
with open('./datas/sat_reference.csv','r') as f:
    sensors = dict([(int(d['SSCNum']),(float(d['Param1']),float(d['Param2']))) for d in csv.DictReader(f)])
# TLEs
with open('./datas/newtle.tce','r') as f:
    tlelines = f.readlines()    
rvs = [rv for rv in [sgp4.io.twoline2rv(tlelines[2*i],tlelines[2*i+1],sgp4.earth_gravity.wgs84) for i in range(int(len(tlelines)/2))] if rv.satnum == 11]   

# roll angles
with open('./datas/00011_PAN_RCD_FOV.sp','r') as f:
    header = [f.readline() for n in range(6)]
epoch_ref = time.strptime(header[3][16:-1],"%d %b %Y %H:%M:%S.%U0")
ors = np.genfromtxt('./datas/00011_PAN_RCD_FOV.sp',skip_header = 6,skip_footer=1)
ors[:,0] = ors[:,0]+time.mktime(epoch_ref) # convert into common epoch

# aperture opening intervals
with open('./datas/00011_PAN_RCD_FOV.int','r') as f:
    aps = [[dt.datetime.strptime(s,"%Y/%m/%d %H:%M:%S").timestamp() for s in a] for a in [re.findall(r'\"(.+?)\"',l) for l in f] if len(a)==2]

# matching aperture times to other data elements
rvi = np.searchsorted([rv.epoch.timestamp() for rv in rvs],aps)
ori = np.searchsorted(ors[:,0],aps)



w = shapefile.Writer(shapefile.POLYGON)
for i in range(len(aps)):
    fp = fnFootprint(*aps[i],rvs[rvi[i][0]],ors[ori[i][0],3],*sensors[11])
    w.poly([fp])
w.save('./shapefiles/FOV_11.shp')
    
#w = shapefile.Writer(shapefile.POLYGON)
#w.poly([fnFootprint(apo,apc,rv,ra,hdx,hdy)])
##w.field('TIMESTAMP',times[i])
##w.field('SATNUM',rvs[rvi[i]].satnum)
##w.record("{:d}_{:d}".format(rvs[rvi[i]].satnum,int(times[i])),'Polygon')
#w.save('./shapefiles/FOV.shp')